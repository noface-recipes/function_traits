#include <type_traits>
#include <iostream>
#include <iomanip>

#include <function_traits.hpp>

using namespace std;
using namespace fn_traits;

int myFunction( int, float ) {
    return 0;
}

int main() {
    typedef function_traits<decltype( myFunction )> traits;

    auto myFunctionArg0IsInt = is_same<int, typename traits::template arg<0>::type>::value;
    cout << boolalpha <<
            "is_same<int, typename traits::template arg<0>::type>::value -> " << myFunctionArg0IsInt
         << endl; //true

    auto cb = []() -> double {
        return 10.0;
    };

    auto cbReturnIsDouble = is_same<double, typename function_traits<decltype( cb )>::result_type>::value;
    cout << boolalpha <<
            "is_same<double, typename function_traits<decltype( cb )>::result_type>::value -> "
                << cbReturnIsDouble << endl; //true

    return 0;
}
