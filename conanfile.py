from conans import ConanFile, tools

import os
from os import path

class Recipe(ConanFile):
    name           = "function_traits"
    description    = "header only library with c++ traits for functions and functor types"
    commit_hash    = "3bd311e"
    code_timestamp = "20171225"  # Date of package commit
    version        = code_timestamp
    license        = "unlicense"
    url            = "https://gitlab.com/noface-recipes/function_traits-conan"
    repo           = "https://github.com/novacrazy/function_traits"

    build_policy = "missing" #header-only, so it is ok

    ZIP_URL      = "{}/zipball/{}".format(repo, commit_hash)
    ZIP_DIR_NAME = "novacrazy-{}-{}".format(name, commit_hash)
    FILE_SHA     = "76b0d4dc80f2197fa1f9728a7cc462a0dee16ba756b4943d9c42dbfd994beb0b"
    SOURCES_DIR  = "sources"

    repo_name   = name

    def source(self):
        self.output.info("Downloading zipball from github repository: '{}'".format(self.ZIP_URL))

        zip_name = "{}.zip".format(self.name)

        tools.download(self.ZIP_URL, zip_name)
        tools.check_sha256(zip_name, self.FILE_SHA)

        tools.unzip(zip_name)
        os.unlink(zip_name)

        os.rename(self.ZIP_DIR_NAME, self.SOURCES_DIR)

    def package(self):
        self.copy("*", dst="include", src=path.join(self.SOURCES_DIR, "include"))

    def package_info(self):
        pass

