# Function_traits-conan

[Conan.io](https://www.conan.io/) package for https://github.com/novacrazy/function_traits

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
function_traits [repository](https://github.com/novacrazy/function_traits) to instruction in
how to use the library.


## About function_traits

Function traits is a header only library with c++ traits for functions and related types.

See the repository for more info.
